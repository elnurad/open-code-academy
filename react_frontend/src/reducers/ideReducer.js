import { IDETypes } from "../constants/actionTypes";

const initialState = {
    lines: [],
    error: null
};

function lines(state = [], action) {
    switch (action.type) {
      case IDETypes.WRITE_TERMINAL:
        let head = state.slice(0, state.length - 1),
            tail = state[state.length - 1] || [];
        return [...head, [...tail, action.chunk]];
  
      case IDETypes.BREAK_TERMINAL:
        return [...state, []];
    }
    return state;
};

export function terminal(state = initialState, action) {
    switch (action.type) {
    case IDETypes.WRITE_TERMINAL:
    case IDETypes.BREAK_TERMINAL:
        const l = lines(state.lines, action);
        return Object.assign({}, state, { lines: l });

    case IDETypes.ERROR_TERMINAL:
        return Object.assign({}, state, { error: action.line });
    
        case IDETypes.FLUSH_TERMINAL:
        return initialState;

    case IDETypes.EXIT_TERMINAL:
        action.worker.terminate()
        return state
    }
    return state;
};
