import 'xterm/src/xterm.css'
import 'xterm/src/addons/fullscreen/fullscreen.css'
import 'xterm/dist/addons/terminado/terminado.js';
import 'xterm/dist/addons/fit/fit.js';
import 'xterm/dist/addons/attach/attach.js';
import './addons/attach.js';
import React from "react"
import { XTerm } from 'react-xterm'
import { ResizableBox } from 'react-resizable'
import * as throttle from 'lodash.throttle'
import axios from 'axios'


class WebTerminal extends React.Component {
  
  componentDidMount() {
    runFakeTerminal(this.refs.xterm);
  }

  resize = () => {
    this.refs.xterm && this.refs.xterm.fit()
  }

  throttleConsoleResize = (size)  => {
    throttle(resize, 50)
  }

  render() {
    return <div>
      <ResizableBox width={200} height={200} onResize={this.throttleConsoleResize} style={{
        overflow: 'hidden'
      }}>
				<XTerm ref='xterm' style={{
					addons:['terminado', 'attach', 'fit', 'fullscreen', 'search'],
					overflow: 'hidden',
					position: 'relative',
					width: '100%',
					height: '100%'
				}} />
      </ResizableBox>
    </div>
  }
}

function runFakeTerminal(xterm) {
  const term = xterm.getTerminal();
  var shellprompt = '$ ';

  const res = axios.get("http://localhost:8700/new_json/3333").then(r => console.log(r))
  //const socket = new WebSocket('ws://localhost:8700/_websocket/');
  //socket.onopen = () => {
	//	xterm.csphereAttach(socket);
	//	term.fit();
	//}

  function prompt () {
    xterm.write('\r\n' + shellprompt);
  };
  xterm.writeln('Welcome to xterm.js');
  xterm.writeln('This is a local terminal emulation, without a real terminal in the back-end.');
  xterm.writeln('Type some keys and commands to play around.');
  xterm.writeln('');
  prompt();

  term.on('key', function (key, ev) {
    var printable = (
      !ev.altKey && !ev.ctrlKey && !ev.metaKey
    );

    if (ev.keyCode == 13) {
      prompt();
    // } else if (ev.keyCode == 8) {
      // Do not delete the prompt
      // if (term['x'] > 2) {
      //   xterm.write('\b \b');
      // }
    } else if (printable) {
      xterm.write(key);
    }
  });

  term.on('paste', function (data, ev) {
    xterm.write(data);
  });
}

export default WebTerminal;
