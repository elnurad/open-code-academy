const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const PROD = process.env.NODE_ENV === 'production';
const DEV = !PROD;
const DevToolPlugin = PROD
    ? webpack.SourceMapDevToolPlugin
    : webpack.EvalSourceMapDevToolPlugin;

const config = {
    entry: ["./src/index"],
    output: {
        filename: DEV ? "bundle.js" : "bundle.[hash].js",
        path: path.resolve(__dirname, "dist"),
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.js.map$/,
                use: 'ignore-loader',
            },
            {
                test: /\.js$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.worker\.js$/,
                use: { loader: 'worker-loader' }
            },
            {
                test: /\.css$/,
                use: [
                    DEV ? 'style-loader' : MiniCssExtractPlugin.loader,
                    'css-loader',
                ],
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 20000
                        }
                    },
                    "image-webpack-loader"
                ]
            },
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            },
            { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: DEV ? 'styles.css' : 'styles.[contenthash:6].css',
            chunkFilename: DEV ? 'styles.css' : 'styles.[id].css',
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: true,
            filename: 'index.html'
        }),
        new DevToolPlugin({
            filename: "[file].map"
        }),
        new webpack.IgnorePlugin(/\.(test|spec)\./)
    ], devServer: {
        disableHostCheck: true,
        historyApiFallback: true,
    },
    mode: PROD ? 'production' : 'development',
    externals: {
        'node-pty': 'xterm',
        'net': 'xterm',
        'fs': 'xterm',
        'express-ws': 'xterm'
    },
};

!PROD && (config.devtool = 'source-map');

PROD && config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        compressor: {
            warnings: false,
        }
    })
);

module.exports = config;
